﻿using Microsoft.Build.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnusedHeaderDetector
{
    class BuildLogger : Microsoft.Build.Utilities.Logger
    {
        IList<String> messages = new List<String>();

        public override void Initialize(IEventSource eventSource)
        {
            this.messages.Clear();

            eventSource.ProjectStarted += new ProjectStartedEventHandler(OnProjectStarted);
            eventSource.TaskStarted += new TaskStartedEventHandler(OnTaskStarted);
            eventSource.MessageRaised += new BuildMessageEventHandler(OnMessageRaised);
            eventSource.WarningRaised += new BuildWarningEventHandler(OnWarningRaised);
            eventSource.ErrorRaised += new BuildErrorEventHandler(OnErrorRaised);
            eventSource.ProjectFinished += new ProjectFinishedEventHandler(OnProjectFinished);
        }

        private void OnProjectFinished(object sender, ProjectFinishedEventArgs e)
        {
            LogMessage(e.Message);
        }

        private void OnErrorRaised(object sender, BuildErrorEventArgs e)
        {
            LogMessage(e.Message);
        }

        private void OnWarningRaised(object sender, BuildWarningEventArgs e)
        {
            LogMessage(e.Message);
        }

        private void OnMessageRaised(object sender, BuildMessageEventArgs e)
        {
            LogMessage(e.Message);
        }

        private void OnTaskStarted(object sender, TaskStartedEventArgs e)
        {
            LogMessage(e.Message);
        }

        private void OnProjectStarted(object sender, ProjectStartedEventArgs e)
        {
            LogMessage(e.Message);
        }

        private void LogMessage(String message)
        {
            messages.Add(message);
        }


        public override void Shutdown()
        {
        }
    }
}
