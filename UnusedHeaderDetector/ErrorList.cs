﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnusedHeaderDetector
{
    class ErrorList
    {
        private readonly ErrorListProvider errorListProvider = null;
        private readonly IVsHierarchy hierarchy = null;

        public ErrorList(IServiceProvider serviceProvider, ErrorListProvider errorListProvider, String projectUniqueName)
        {
            this.errorListProvider = errorListProvider;

            IVsSolution vsSolution = serviceProvider.GetService(typeof(SVsSolution)) as IVsSolution;
            ErrorHandler.ThrowOnFailure(vsSolution.GetProjectOfUniqueName(projectUniqueName, out this.hierarchy));
        }

        public void Clear()
        {
            this.errorListProvider.Tasks.Clear();
        }

        public void AddReport(UnusedHeaderReport report)
        {
            ErrorTask task = new ErrorTask();
            task.Category = TaskCategory.User;
            task.ErrorCategory = TaskErrorCategory.Warning;

            task.Line = report.LineNumber - 1;
            task.Column = 0;
            task.Document = report.FilePath;
            task.Text = String.Format("Unused header: {0}", report.LineContent);
            task.HierarchyItem = this.hierarchy;
            task.Navigate += new EventHandler(OnErrorNavigate);

            this.errorListProvider.Tasks.Add(task);
        }

        /// <summary>
        /// Callback raised when a double click on an error is performed. We just want to go to the right file at the right line.
        /// </summary>
        private void OnErrorNavigate(object sender, EventArgs e)
        {
            ErrorTask errorTask = sender as ErrorTask;
            if (errorTask != null)
            {
                errorTask.Line += 1;
                this.errorListProvider.Navigate(errorTask, new Guid(EnvDTE.Constants.vsViewKindCode));
                errorTask.Line -= 1;
            }
        }
    }
}
