﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnusedHeaderDetector
{
    class File
    {
        private readonly String path;
        private readonly IList<int> includeLines;
        private readonly IDictionary<int, String> lineContents;

        public File(String filePath)
        {
            this.path = filePath;
            this.includeLines = new List<int>();
            this.lineContents = new Dictionary<int, String>();

            if(System.IO.File.Exists(path))
            {
                using (System.IO.StreamReader streamReader = new System.IO.StreamReader(path))
                {
                    int lineNumber = 1;
                    string nextLine = streamReader.ReadLine();
                    while(nextLine != null)
                    {
                        if(nextLine.StartsWith("#include"))
                        {
                            this.includeLines.Add(lineNumber);
                            lineContents[lineNumber] = nextLine;
                        }

                        nextLine = streamReader.ReadLine();
                        lineNumber++;
                    }
                }
            }
        }

        public String Path
        {
            get
            {
                return this.path;
            }
        }

        public IList<int> IncludeLines
        {
            get
            {
                return this.includeLines;
            }
        }

        public String GetLineContent(int lineNumber)
        {
            String lineContent = null;

            if(this.lineContents.ContainsKey(lineNumber))
            {
                lineContent = this.lineContents[lineNumber];
            }

            return lineContent;
        }
    }
}
