﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnusedHeaderDetector
{
    /// <summary>
    /// RAII class to create a backup of a file and restore the backup when this object is released.
    /// 
    /// The correct usage is to use the object in a "using" statement.
    /// </summary>
    class FileBackup : IDisposable
    {
        private readonly String originalFilePath = null;
        private readonly String backupFilePath = null;

        public FileBackup(String filePath)
        {
            this.originalFilePath = filePath;
            this.backupFilePath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), System.IO.Path.GetTempFileName());
            System.IO.File.Copy(filePath, this.backupFilePath, true);
        }

        public void Dispose()
        {
            System.IO.File.Copy(this.backupFilePath, this.originalFilePath, true);
            System.IO.File.Delete(this.backupFilePath);
        }

        public String BackupFilePath
        {
            get
            {
                return this.backupFilePath;
            }
        }
    }
}
