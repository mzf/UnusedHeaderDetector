﻿// Guids.cs
// MUST match guids.h
using System;

namespace UnusedHeaderDetector
{
    static class GuidList
    {
        public const string guidUnusedHeaderDetectorPkgString = "2e67f651-eb69-46d4-89de-bbcb7e415c21";
        public const string guidUnusedHeaderDetectorCmdSetString = "ffdd4204-7461-459e-b3ae-e432c5d45cb6";

        public static readonly Guid guidUnusedHeaderDetectorCmdSet = new Guid(guidUnusedHeaderDetectorCmdSetString);
    };
}