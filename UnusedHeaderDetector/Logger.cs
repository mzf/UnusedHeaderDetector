﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnusedHeaderDetector
{
    class Logger
    {
        private readonly IVsOutputWindowPane outputWindowPanel = null;
        public Logger(IVsOutputWindow outputWindow)
        {
            if (outputWindow != null)
            {
                Guid guidGeneralPane = VSConstants.GUID_OutWindowDebugPane;
                outputWindow.GetPane(ref guidGeneralPane, out this.outputWindowPanel);
            }
        }

        public void Log(String message)
        {
            if (this.outputWindowPanel != null)
            {
                this.outputWindowPanel.OutputString(String.Format("{0}\n", message));
                this.outputWindowPanel.Activate();
            }
        }
    }
}
