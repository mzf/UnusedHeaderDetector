﻿using EnvDTE;
using Microsoft.Build.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnusedHeaderDetector
{
    class UnusedHeaderDetector
    {
        private readonly IList<File> files;
        private readonly String projectFilePath;
        private readonly Logger logger;

        public UnusedHeaderDetector(String projectFilePath, IList<String> filePaths, Logger logger)
        {
            this.files = new List<File>();
            this.projectFilePath = projectFilePath;
            this.logger = logger;

            foreach (String filePath in filePaths)
            {
                files.Add(new File(filePath));
            }
        }

        public IList<UnusedHeaderReport> Detect()
        {
            IList<UnusedHeaderReport> reports = new List<UnusedHeaderReport>();
            foreach(File file in this.files)
            {
                logger.Log(String.Format("Processing file: {0}", file.Path));

                // First check that the file compiles successfully.
                if (IsCompilationOfFileSuccessfull(file.Path))
                {
                    foreach (int lineNumber in file.IncludeLines)
                    {
                        bool doesFileCompileWithoutLine = DoesFileCompileWithoutLine(file.Path, lineNumber);
                        if (doesFileCompileWithoutLine)
                        {
                            reports.Add(new UnusedHeaderReport(file.Path, lineNumber, file.GetLineContent(lineNumber)));
                        }
                    }
                }
                else
                {
                    logger.Log(String.Format("Build of file failed, skipping {0}.", file.Path));
                }
            }

            return reports;
        }

        private bool DoesFileCompileWithoutLine(string filePath, int lineNumber)
        {
            bool isCompilationSuccessfull = false;

            // Create a backup of the file before modifying the original file.
            using (FileBackup fileBackup = new FileBackup(filePath))
            {
                // Remove line.
                using (System.IO.StreamReader originalReader = new System.IO.StreamReader(fileBackup.BackupFilePath))
                {
                    using (System.IO.StreamWriter writer = new System.IO.StreamWriter(filePath))
                    {
                        int currentLineNumber = 1;
                        string nextLine = originalReader.ReadLine();
                        while (nextLine != null)
                        {
                            if (currentLineNumber != lineNumber)
                            {
                                writer.WriteLine(nextLine);
                            }

                            nextLine = originalReader.ReadLine();
                            currentLineNumber++;
                        }
                    }
                }

                // Compile.
                isCompilationSuccessfull = IsCompilationOfFileSuccessfull(filePath);
            }

            return isCompilationSuccessfull;
        }

        private bool IsCompilationOfFileSuccessfull(string filePath)
        {
            Dictionary<string, string> globalProperties = new Dictionary<string, string>();
            Microsoft.Build.Execution.BuildRequestData buildRequestData = new Microsoft.Build.Execution.BuildRequestData(this.projectFilePath, globalProperties, null, new string[] { "Build" }, null);

            Microsoft.Build.Execution.BuildParameters buildParameters = new Microsoft.Build.Execution.BuildParameters(new Microsoft.Build.Evaluation.ProjectCollection());

            // Create a logger for debug purpose.
            BuildLogger buildLogger = new BuildLogger();
            List<ILogger> buildLoggers = new List<ILogger>();
            buildLoggers.Add(buildLogger);
            buildParameters.Loggers = buildLoggers;

            Microsoft.Build.Execution.BuildResult result = Microsoft.Build.Execution.BuildManager.DefaultBuildManager.Build(buildParameters, buildRequestData);
            Microsoft.Build.Execution.BuildResultCode resultCode = result.OverallResult;
            return resultCode == Microsoft.Build.Execution.BuildResultCode.Success;
        }


    }
}
