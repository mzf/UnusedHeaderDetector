﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.ComponentModel.Design;
using Microsoft.Win32;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.Shell;
using EnvDTE;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace UnusedHeaderDetector
{
    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    ///
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the 
    /// IVsPackage interface and uses the registration attributes defined in the framework to 
    /// register itself and its components with the shell.
    /// </summary>
    // This attribute tells the PkgDef creation utility (CreatePkgDef.exe) that this class is
    // a package.
    [PackageRegistration(UseManagedResourcesOnly = true)]
    // This attribute is used to register the information needed to show this package
    // in the Help/About dialog of Visual Studio.
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    // This attribute is needed to let the shell know that this package exposes some menus.
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [Guid(GuidList.guidUnusedHeaderDetectorPkgString)]
    public sealed class UnusedHeaderDetectorPackage : Package, IDisposable
    {
        private readonly ErrorListProvider errorListProvider = null;

        /// <summary>
        /// Default constructor of the package.
        /// Inside this method you can place any initialization code that does not require 
        /// any Visual Studio service because at this point the package object is created but 
        /// not sited yet inside Visual Studio environment. The place to do all the other 
        /// initialization is the Initialize method.
        /// </summary>
        public UnusedHeaderDetectorPackage()
        {
            this.errorListProvider = new ErrorListProvider(this);
        }



        /////////////////////////////////////////////////////////////////////////////
        // Overridden Package Implementation
        #region Package Members

        /// <summary>
        /// Initialization of the package; this method is called right after the package is sited, so this is the place
        /// where you can put all the initialization code that rely on services provided by VisualStudio.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            // Add our command handlers for menu (commands must exist in the .vsct file)
            OleMenuCommandService mcs = GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if ( null != mcs )
            {
                // Create the command for the menu item.
                CommandID menuCommandID = new CommandID(GuidList.guidUnusedHeaderDetectorCmdSet, (int)PkgCmdIDList.cmdidTestCommand);
                MenuCommand menuItem = new MenuCommand(MenuItemCallback, menuCommandID );
                mcs.AddCommand( menuItem );
            }
        }
        #endregion

        /// <summary>
        /// This function is the callback used to execute a command when the a menu item is clicked.
        /// See the Initialize method to see how the menu item is associated to this function using
        /// the OleMenuCommandService service and the MenuCommand class.
        /// </summary>
        private async void MenuItemCallback(object sender, EventArgs e)
        {
            IVsOutputWindow outputWindow = GetService(typeof(SVsOutputWindow)) as IVsOutputWindow;
            Logger logger = new Logger(outputWindow);

            logger.Log("---- Start detecting unused headers ----");

            DTE dte = GetService(typeof(SDTE)) as DTE;
            VisualStudioEnvironment vsEnvironment = new VisualStudioEnvironment(dte);

            UnusedHeaderDetector detector = new UnusedHeaderDetector(vsEnvironment.ProjectFullName, vsEnvironment.SelectedFileNames, logger);
            IList<UnusedHeaderReport> reports = await System.Threading.Tasks.Task.Run(() => detector.Detect());

            ErrorList errorList = new ErrorList(this, this.errorListProvider, vsEnvironment.ProjectUniqueName);
            ProcessReports(reports, logger, errorList);

            logger.Log("---- End detecting unused headers ----");

        }

        private void ProcessReports(IList<UnusedHeaderReport> reports, Logger logger, ErrorList errorList)
        {
            errorList.Clear();
            foreach (UnusedHeaderReport report in reports)
            {
                errorList.AddReport(report);
                logger.Log(String.Format("Unused header: {0} in file {1} line {2}.", report.LineContent, report.FilePath, report.LineNumber));
            }

            if (reports.Count == 0)
            {
                logger.Log("No unused header detected.");
            }
        }

        public void Dispose()
        {
            this.errorListProvider.Dispose();
        }
    }
}
