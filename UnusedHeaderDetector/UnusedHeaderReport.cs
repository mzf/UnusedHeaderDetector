﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnusedHeaderDetector
{
    class UnusedHeaderReport
    {
        private readonly string file;
        private readonly int lineNumber;
        private readonly string lineContent;

        public UnusedHeaderReport(String file, int lineNumber, String lineContent)
        {
            this.file = file;
            this.lineNumber = lineNumber;
            this.lineContent = lineContent;
        }

        public String FilePath
        {
            get
            {
                return this.file;
            }
        }

        public int LineNumber
        {
            get
            {
                return this.lineNumber;
            }
        }

        public String LineContent
        {
            get
            {
                return this.lineContent;
            }
        }
    }
}
