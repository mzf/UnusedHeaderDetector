﻿using EnvDTE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnusedHeaderDetector
{
    class VisualStudioEnvironment
    {
        private readonly String projectFullName = "";
        private readonly String projectUniqueName = "";
        private readonly IList<String> selectedFileNames = new List<String>();

        public VisualStudioEnvironment(DTE dte)
        {
            Array projects = dte.ActiveSolutionProjects as Array;
            if (projects.Length > 0)
            {
                Project project = projects.GetValue(0) as Project;
                this.projectFullName = project.FullName;
                this.projectUniqueName = project.UniqueName;
            }

            foreach (SelectedItem selectedItem in dte.SelectedItems)
            {
                if (selectedItem.ProjectItem != null)
                {
                    ProjectItem projectItem = selectedItem.ProjectItem;
                    Property fullPathProperty = projectItem.Properties.Item("FullPath");
                    if (fullPathProperty != null)
                    {
                        String fullPath = fullPathProperty.Value.ToString();
                        selectedFileNames.Add(fullPath);
                    }
                }
            }
        }

        public String ProjectFullName
        {
            get
            {
                return this.projectFullName;
            }
        }

        public String ProjectUniqueName
        {
            get
            {
                return this.projectUniqueName;
            }
        }

        public IList<String> SelectedFileNames
        {
            get
            {
                return this.selectedFileNames;
            }
        }

    }
}
